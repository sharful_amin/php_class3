<?php








#function

#variable scope

/*
    // ekta kaj korbe
    // database connection-----function
    // data update ----function

    // Pre-define function
    // user define function

    // parameter
    // arguments

    // variable scope
*/



// function printName(){
//     echo "Sharful";

// }

// printName();

function sumTwoNumbers($numberOne,$numberTwo=50){
        $result = $numberOne + $numberTwo;
        echo $result;
}

sumTwoNumbers(40);
sumTwoNumbers(50,10);
sumTwoNumbers(15,55);


$number = 100;

function printNumber(){
    global $number;
    echo $number;
    $name="PONDIT";
    echo $name;
}

printNumber();

echo $number;
// echo $name;



$course = "Laravel";

function courseInfo(){
    $course = "PHP";
}

function companyInfo(){
    $companyName = "SEIP";
}

echo $course;


function outer(){
    $name = "Karim";

     $inner = function() use($name){ //inner//
        // global $name;
        echo $name;
    };
    $inner();  
}
outer();

// Find the largest number.

// $a = 20;
// $b = 10;
// $c = 13;

// if($a>$b && $a>$c){
//     echo "$a is the largest number.";
// }
// elseif($b>$c && $b>$a){
//     echo "$b is the largest number.";
// }
// else{
//     echo "$c is the largest number.";
// }

// Find the largest number with using function.

function findLargestNumber($a,$b,$c){
    if($a>$b && $a>$c){
        echo "$a is the largest number.";
    }
    elseif($b>$c && $b>$a){
        echo "$b is the largest number.";
    }
    else{
        echo "$c is the largest number.";
    }
}

findLargestNumber(20,45,30);


// Find sum of two numbers.

function addTwoNumbers($a,$b){
    $result = $a + $b;
    return $result;
}

$output = addTwoNumbers(100,400);
echo $output;













?>